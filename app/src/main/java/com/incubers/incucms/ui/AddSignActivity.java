package com.incubers.incucms.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.incubers.incucms.R;
import com.incubers.incucms.utils.SendGmail;

import java.io.File;
import java.io.FileOutputStream;

import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TAG;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;

public class AddSignActivity extends AppCompatActivity {
    private int PERMISSION_REQUEST_CODE = 5;

    ImageView signView;
    Button mClear, mGetSign, mCancel;
    LinearLayout mContent;
    View view;
    signature mSignature;
    Bitmap bitmap;
    // Creating Separate Directory for saving Generated Images
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Incu CMS/User Signature/";
    String pic_name = "usersign";
    String StoredPath = DIRECTORY + pic_name + ".png";
    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == mClear) {
                Log.v(TAG, "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
                mGetSign.setVisibility(View.GONE);
                mClear.setEnabled(false);
                mClear.setVisibility(View.GONE);
            } else if (v == mGetSign) {
                Log.v(TAG, "Panel saved");
                view.setDrawingCacheEnabled(true);
                mSignature.save(view, StoredPath);
                // Calling the same class
                recreate();

            } else if (v == mCancel) {
                Log.v(TAG, "Panel Canceled");
                finish();
                // Calling the BillDetailsActivity
                /*Intent intent = new Intent(AddSignActivity.this, DigitalToolsActivity.class);
                startActivity(intent);*/
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sign);
        mContent = findViewById(R.id.canvasLayout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermission();
        }
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        Log.e(TAG, value);
        //view sign
        signView = findViewById(R.id.signedImg);
        Bitmap bitmap = BitmapFactory.decodeFile(StoredPath);
        signView.setImageBitmap(bitmap);
        signView.setOnClickListener(view -> {
            mContent.setVisibility(View.VISIBLE);
            signView.setVisibility(View.GONE);

        });
        mSignature = new signature(getApplicationContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = findViewById(R.id.clear);
        mGetSign = findViewById(R.id.getsign);
        File file = new File(StoredPath);
        if(file.exists()){
            mGetSign.setText("Update");
        }
        mGetSign.setEnabled(false);
        mGetSign.setVisibility(View.GONE);
        mClear.setEnabled(false);
        mClear.setVisibility(View.GONE);
        mCancel = findViewById(R.id.cancel);
        view = mContent;
        mGetSign.setOnClickListener(onButtonClick);
        mClear.setOnClickListener(onButtonClick);
        mCancel.setOnClickListener(onButtonClick);
        // Method to create Directory, if the Directory doesn't exists
        file = new File(DIRECTORY);
        file.mkdirs();
        boolean isDirectoryCreated = file.mkdir();

    }

    public void getPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(this, "All required permission granted", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(AddSignActivity.this);
                builder1.setMessage("You have denied the permission which is required to run this feature of app please go to settings and provide all permission");
                builder1.setPositiveButton(
                        "Open Settings",
                        (dialogInterface, i) -> {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            finish();
                        });
                builder1.setCancelable(false);
                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        }
    }
    public class signature extends View {

        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private final RectF dirtyRect = new RectF();
        private Paint paint = new Paint();
        private Path path = new Path();
        private float lastTouchX;
        private float lastTouchY;

        public signature(Context context, AttributeSet attrs) {

            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v, String StoredPath) {
            Log.v(TAG, "Width: " + v.getWidth());
            Log.v(TAG, "Height: " + v.getHeight());
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);

            }
            Canvas canvas = new Canvas(bitmap);
            try {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);
                // Convert the output file to Image such as .png
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                //Creating SendMail object
                String fileName = "User Signature";
                String message = "Hello, here is your signature";
                SendGmail sm = new SendGmail(AddSignActivity.this, message, StoredPath, fileName,value);
                //Executing sendmail to send email
                sm.execute();
                Toast toast = Toast.makeText(getApplicationContext()
                        , "Successfully Saved !!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                mFileOutStream.flush();
                mFileOutStream.close();

            } catch (Exception e) {
                Log.v(TAG, "Exception: " + e.toString());
            }

        }

        public void clear() {
            path.reset();
            invalidate();
            mGetSign.setEnabled(false);
            mGetSign.setVisibility(View.GONE);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);
            mGetSign.setVisibility(View.VISIBLE);
            mClear.setEnabled(true);
            mClear.setVisibility(View.VISIBLE);
/*
            Log.v(TAG, "mSignature: " + event);
*/

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {

            Log.v(TAG, string);

        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }


}