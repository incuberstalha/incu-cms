package com.incubers.incucms.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.incubers.incucms.R;
import com.incubers.incucms.utils.SendGmail;
import com.incubers.incucms.utils.actions.AlertBox;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TAG;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;
public class ReceiptScanActivity extends AppCompatActivity {
    private final static int ALL_PERMISSIONS_RESULT = 107;
    PDFView pdfView;
    //save file dir
    String exportDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Incu CMS/Receipt  Scanner/";
    String newFileName = UUID.randomUUID().toString() + ".pdf";
    // file which have name and path
    File file = new File(exportDir, newFileName);
    File dir = new File(exportDir);
    Uri picUri;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    public ReceiptScanActivity() throws IOException {
    }

    private File createImageFile() throws IOException {
        // for genrating pdf name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String pdfFileName = "Incu" + timeStamp + "_";
        return File.createTempFile(
                pdfFileName, ".pdf"
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipt_scan);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        Log.e(TAG, value);
        TextView button = findViewById(R.id.reciptscanbttn);
        button.setOnClickListener(view -> CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(ReceiptScanActivity.this));
        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[0]), ALL_PERMISSIONS_RESULT);
        }

    }

    private void convertImage(String path) throws IOException, DocumentException {
        Document document = new Document();
        OutputStream fOut = null;
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file.createNewFile();
        fOut = new FileOutputStream(file);
        fOut.flush();
        fOut.close();
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        Image img = null;
        img = Image.getInstance(path);
        img.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
        img.setAlignment(Element.ALIGN_CENTER);
        Log.d(TAG, "String " + path);
        document.add(img);
        document.close();
        Log.d(TAG, "pdf : " + document);
        emailFile(String.valueOf(file));
        pdfView = findViewById(R.id.reciptpdfView);
        pdfView.setVisibility(View.VISIBLE);
        pdfView.fromFile(file).load();
        Toast.makeText(this, "Converted Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String filePath = String.valueOf(result.getUri());
                try {
                    convertImage(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("pic_uri", picUri);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        picUri = savedInstanceState.getParcelable("pic_uri");
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void emailFile(String StoredPath) throws IOException {
        AlertBox alertBox = new AlertBox(ReceiptScanActivity.this);
        String message = "Hello, here is scanned receipt";
        String fileName = createImageFile().getName();
        SendGmail sm = new SendGmail(ReceiptScanActivity.this, message, StoredPath, fileName,value);
        //Executing sendmail to send email
        sm.execute();
    }
}

