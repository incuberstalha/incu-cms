package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class ReportingLocationActivity extends AppCompatActivity {
    Button newRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporting_location);
        newRepo = findViewById(R.id.new_reporting);
        newRepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReportingLocationActivity.this, NewReportingActivity.class);
                startActivity(intent);
            }
        });
    }
}
