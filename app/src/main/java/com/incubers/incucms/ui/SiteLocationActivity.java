package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class SiteLocationActivity extends AppCompatActivity {
    ImageButton resButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_location);
       /* WebView webView = findViewById(R.id.webView1);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("https://mykingsspeech.com/grandrehabber-test/");

            }*/
        resButton = findViewById(R.id.viewReporting);
        resButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SiteLocationActivity.this, ReportingLocationActivity.class);
                startActivity(intent);
            }
        });

    }
}