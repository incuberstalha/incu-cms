package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class FinancialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_financial);
    }

    public void financialButton(View view) {
        Intent intent = new Intent(this, FinancialHomeActivity.class);
        startActivity(intent);
    }

    public void bankingButton(View view) {
        Intent intent = new Intent(this, BankingActivity.class);
        startActivity(intent);
    }

    public void hrButton(View view) {
        Intent intent = new Intent(this, HRActivity.class);
        startActivity(intent);
    }

    public void Reports(View view) {
        Intent intent = new Intent(this, ReportsActivity.class);
        startActivity(intent);
    }
}
