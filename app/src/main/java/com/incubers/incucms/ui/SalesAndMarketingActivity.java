package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class SalesAndMarketingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_and_marketing);
    }

    public void Marketing(View view) {
        Intent intent = new Intent(this, MarketingActivity.class);
        startActivity(intent);

    }

    public void Leads(View view) {
        Intent intent = new Intent(this, LeadsActivity.class);
        startActivity(intent);

    }

    public void Sales(View view) {
        Intent intent = new Intent(this, SalesActivity.class);
        startActivity(intent);
    }

    public void siteVisit(View view) {
        Intent intent = new Intent(this, SiteVisitsActivity.class);
        startActivity(intent);
    }

    public void contactDirButton(View view) {
        Intent intent = new Intent(this, ContactsDirActivity.class);
        startActivity(intent);


    }


}
