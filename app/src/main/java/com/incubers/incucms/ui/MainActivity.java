package com.incubers.incucms.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.incubers.incucms.R;
import com.incubers.incucms.ui.auth.ClientActivity;
import com.incubers.incucms.utils.actions.AlertFragment;
import com.incubers.incucms.utils.actions.VarHelper;

import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;
public class MainActivity extends AppCompatActivity {
    private static final int PERIOD = 2000;
    private long lastPressedTime;
    private int PERMISSION_REQUEST_CODE = 13;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!isConnected()) {
            // Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            alertUserAboutError();
        }
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        Log.e(VarHelper.TAG, value);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermission();
        }
    }

    private void alertUserAboutError() {
        Bundle messageArgs = new Bundle();
        messageArgs.putString(AlertFragment.TITLE_ID, "No Internet Connection");
        messageArgs.putString(AlertFragment.MESSAGE_ID, "App won't work without internet connection");
        AlertFragment dialog = new AlertFragment();
        dialog.setArguments(messageArgs);
        dialog.show(getSupportFragmentManager(), "error_dialog");
    }

    public boolean isConnected() {
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return connected;
        } catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return connected;
    }

    public void getPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length == 4 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "All required permission granted", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage("You have denied the permission which is required to run some feature of the app");
                builder1.setPositiveButton(
                        "Provide permission",
                        (dialogInterface, i) -> {

                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        });
                builder1.setCancelable(true);
                builder1.setNegativeButton("Never mind", (dialog, which) -> dialog.cancel());
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
    }

    public void salesAndMarketing(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, SalesAndMarketingActivity.class);
            startActivity(intent);
        }
    }

    public void financialCompany(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, FinancialActivity.class);
            startActivity(intent);
        }
    }

    public void workManage(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, WorkManagmentActivity.class);
            startActivity(intent);
        }
    }

    public void actionButton(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, ActionActivity.class);
            startActivity(intent);
        }
    }

    public void digitoolsButton(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, DigitalToolsActivity.class);
            startActivity(intent);
        }

    }

    public void hardToolsButton(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, HardToolsActivity.class);
            startActivity(intent);
        }
    }

    public void clientButton(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Choose below");
            builder1.setPositiveButton(
                    "Logout",
                    (dialogInterface, i) -> {
                        SharedPreferences preferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        Toast.makeText(this, "Logout Success", Toast.LENGTH_SHORT).show();
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    });
            builder1.setNegativeButton("Change Email", (dialogInterface, i) -> {
                Intent intent = new Intent(this, ClientActivity.class);
                startActivity(intent);
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (event.getDownTime() - lastPressedTime < PERIOD) {
                    finishAffinity();
                } else {
                    Toast.makeText(getApplicationContext(), "Press again to close the app",
                            Toast.LENGTH_SHORT).show();
                    lastPressedTime = event.getEventTime();
                }
                return true;
            }
        }
        return false;
    }
}
