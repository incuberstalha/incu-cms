package com.incubers.incucms.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
