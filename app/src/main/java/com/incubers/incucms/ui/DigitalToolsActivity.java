package com.incubers.incucms.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;
import com.incubers.incucms.ui.auth.ClientActivity;
import com.incubers.incucms.utils.actions.VarHelper;

import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;
public class DigitalToolsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_tools);
        SharedPreferences sharedPreferences = getSharedPreferences(VarHelper.SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        Log.e(VarHelper.TAG, value);
    }

    public void signatureButton(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, AddSignActivity.class);
            startActivity(intent);

        }
    }

    public void paperMailButton(View view) {
        Intent intent = new Intent(this, PaperMailActivity.class);
        startActivity(intent);
    }

}
