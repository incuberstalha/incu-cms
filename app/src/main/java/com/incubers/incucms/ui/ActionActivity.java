package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class ActionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
    }

    public void calenderButton(View view) {
        Intent intent = new Intent(this, CalenderActivity.class);
        startActivity(intent);
    }

    public void hubEtc(View view) {
        Intent intent = new Intent(this, HubActivity.class);
        startActivity(intent);
    }

    public void notesButton(View view) {
        Intent intent = new Intent(this, NotesActivity.class);
        startActivity(intent);
    }

    public void todoButton(View view) {
        Intent intent = new Intent(this, ToDosActivity.class);
        startActivity(intent);
    }

    public void incuMailButton(View view) {
        Intent intent = new Intent(this, IncuMailActivity.class);
        startActivity(intent);
    }
}
