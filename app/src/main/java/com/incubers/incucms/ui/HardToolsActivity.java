package com.incubers.incucms.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.incubers.incucms.R;
import com.incubers.incucms.ui.auth.ClientActivity;

import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;
public class HardToolsActivity extends AppCompatActivity {
    private int PERMISSION_REQUEST_CODE = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hard_tools);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermission();
        }
    }

    public void getPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length == 4 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "All required permission granted", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(HardToolsActivity.this);
                builder1.setMessage("You have denied the permission which is required to run this part of the app please go to settings and provide all permission");
                builder1.setPositiveButton(
                        "Open Settings",
                        (dialogInterface, i) -> {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        });
                builder1.setCancelable(false);
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
    }
    public void cameraButton(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }


    public void scannerButton(View view) {
        Intent intent = new Intent(this, ScannerActivity.class);
        startActivity(intent);
    }

    public void recorderButton(View view) {
        Intent intent = new Intent(this, RecorderActivity.class);
        startActivity(intent);
    }

    public void geoLocationButton(View view) {
        Intent intent = new Intent(this, GeoLoactionActivity.class);
        startActivity(intent);
    }
    public void receiptScan(View view) {
        if (value == null) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, ReceiptScanActivity.class);
            startActivity(intent);
        }
    }

    public void cardScan(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, ScanCardActivity.class);
            startActivity(intent);
        }
    }

    public void scanDoc(View view) {
        if (value.equals("")) {
            Intent intent = new Intent(this, ClientActivity.class);
            startActivity(intent);
            Toast toast = Toast.makeText(this, "Please add email first", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Intent intent = new Intent(this, DocScanner.class);
            startActivity(intent);
        }
    }
}
