package com.incubers.incucms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;

public class WorkManagmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_managment);
    }

    public void operationButton(View view) {
        Intent intent = new Intent(this, OperationsActivity.class);
        startActivity(intent);
    }

    public void estimateButton(View view) {
        Intent intent = new Intent(this, EstimatesActivity.class);
        startActivity(intent);
    }

    public void projectButton(View view) {
        Intent intent = new Intent(this, ProjectSitesActivity.class);
        startActivity(intent);
    }

    public void ownerclientButton(View view) {
        Intent intent = new Intent(this, OwnersActivity.class);
        startActivity(intent);
    }
}
