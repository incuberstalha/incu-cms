package com.incubers.incucms.ui;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.incubers.incucms.R;

import java.io.ByteArrayOutputStream;

import helper.myDbAdapter;

public class NewReportingActivity extends AppCompatActivity {
    private static final int CAMERA_IMAGES_REQUEST = 124;
    EditText titleTxt, discriptionTxt;
    Button create, cancel;
    Button uploadedImg;
    ImageView upImage;
    Uri videoUri;
    myDbAdapter helper;
    VideoView uploadedVid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reporting);
        findId();
        int STORAGE_PERMISSION_CODE = 23;
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);

       /* String title = titleTxt.getText().toString();
        String discription = discriptionTxt.getText().toString();*/
        helper = new myDbAdapter(this);

        create.setOnClickListener(view -> addUser(view));
        uploadedImg.setOnClickListener(view -> uploadedPhoto());
        uploadedVid.setOnClickListener((view -> uploadedVideo()));
    }

    public void addUser(View view) {
        String t1 = titleTxt.getText().toString();
        String t2 = discriptionTxt.getText().toString();
        if (t1.isEmpty() || t2.isEmpty()) {
            Toast.makeText(this, "Please Provide Details", Toast.LENGTH_SHORT).show();
        } else {
            long id = helper.insertData(t1, t2);
            if (id <= 0) {
                Toast.makeText(this.getApplicationContext(), "Insertion Unsuccessful ", Toast.LENGTH_LONG).show();
//                Toast.makeText(this, "Please Provide Details", Toast.LENGTH_SHORT).show();
                titleTxt.setText("");
                discriptionTxt.setText("");
            } else {
                Toast.makeText(this.getApplicationContext(), "Insertion Successful ", Toast.LENGTH_LONG).show();
//                Toast.makeText(this, "Please Provide Details", Toast.LENGTH_SHORT).show();
                titleTxt.setText("");
                discriptionTxt.setText("");
            }
        }
    }

    private void uploadedVideo() {

    }

    private void uploadedPhoto() {
        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_IMAGES_REQUEST);*/

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        Intent chooserIntent = Intent.createChooser(takePictureIntent, "Capture Image or Video");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takeVideoIntent});
        startActivityForResult(chooserIntent, CAMERA_IMAGES_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_IMAGES_REQUEST) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            upImage.setImageBitmap(thumbnail);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            thumbnail.recycle();
            ContentValues values = new ContentValues();
            values.put("image", byteArray);
//            long id = helper.insertData(values);
//            helper.insert("imageTb", null,values);

        }
    }

    private void playbackRecordedVideo() {

    }


    private void findId() {
        titleTxt = findViewById(R.id.title_media);
        discriptionTxt = findViewById(R.id.title_disc);
        create = findViewById(R.id.create);
        cancel = findViewById(R.id.cancel);
        uploadedImg = findViewById(R.id.img_up);
        uploadedVid = findViewById(R.id.show_vid);
        upImage = findViewById(R.id.show_img);
    }
}
