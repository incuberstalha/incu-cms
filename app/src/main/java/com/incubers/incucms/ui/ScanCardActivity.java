package com.incubers.incucms.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.incubers.incucms.R;
import com.incubers.incucms.utils.SendGmail;
import com.incubers.incucms.utils.actions.AlertBox;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TAG;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;

public class ScanCardActivity extends AppCompatActivity {
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    EditText textView;
    ImageView imageView;
    Uri image_uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bussiness_card);
        textView = findViewById(R.id.textShow);
        imageView = findViewById(R.id.imageV);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        Log.e(TAG, value);
    }

    private void showImageImportant() {
        String[] items = {"Camera", "Gallery"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Select Inage");
        dialog.setItems(items, (dialogInterface, i) -> {
            if (i == 0) {
                pickCamera();
            }

            if (i == 1) {
                pickGallery();
            }
        });
        dialog.create().show();
    }

    private void pickGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPic");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image To text");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }

    public void cardClicked(View view) {
        showImageImportant();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                CropImage.activity(data.getData())
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
            }

            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(image_uri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageView.setImageURI(resultUri);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                TextRecognizer recognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                if (!recognizer.isOperational()) {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                } else {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<TextBlock> items = recognizer.detect(frame);
                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < items.size(); i++) {
                        TextBlock myItem = items.valueAt(i);
                        sb.append(myItem.getValue());
                        sb.append("\n");
                    }

                    textView.setText(sb.toString());
                    textView.setVisibility(View.VISIBLE);

                    String filename = textView.getText().toString();
                    try {
                        saveTextFile(filename);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception exception = result.getError();
                Toast.makeText(getApplicationContext(), "" + exception, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createImageFile() throws IOException {
        // for genrating txt name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String pdfFileName = "Incu" + timeStamp + "_";
        return File.createTempFile(
                pdfFileName,  /* prefix */
                ".txt"    /* directory */
        );
    }

    private void saveTextFile(String filename) throws IOException {
        String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Incu CMS/Card Scanner/";
        String name = createImageFile().getName();
        String StoredPath = DIRECTORY + name;
        File f = new File(DIRECTORY);
        f.mkdirs();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(StoredPath);
            fileOutputStream.write(filename.getBytes());
            fileOutputStream.close();
            emailFile(StoredPath);
            Toast.makeText(getApplicationContext(), "File Saved!!", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "File Not found", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error Saving", Toast.LENGTH_SHORT).show();
        }
    }

    private void emailFile(String StoredPath) throws IOException {
        AlertBox alertBox = new AlertBox(ScanCardActivity.this);
        String message = "Hello, here is the card details";
        String fileName = createImageFile().getName();
        SendGmail sm = new SendGmail(ScanCardActivity.this, message, StoredPath, fileName,value);
        //Executing sendmail to send email
        sm.execute();
    }
}
