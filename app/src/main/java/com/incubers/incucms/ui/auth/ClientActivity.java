package com.incubers.incucms.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.incubers.incucms.R;
import com.incubers.incucms.ui.MainActivity;

import java.util.regex.Pattern;

import static com.incubers.incucms.utils.actions.VarHelper.SHARED_PREFS;
import static com.incubers.incucms.utils.actions.VarHelper.TAG;
import static com.incubers.incucms.utils.actions.VarHelper.TEXT;
import static com.incubers.incucms.utils.actions.VarHelper.value;

public class ClientActivity extends AppCompatActivity {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static Context contextOfApplication;
    EditText et_email;
    TextView showtxt;
    Button btnSave;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String text;

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        contextOfApplication = getApplicationContext();
        et_email = findViewById(R.id.sut_useremail);
        btnSave = findViewById(R.id.sut_userclick);
        showtxt = findViewById(R.id.show_txt);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        value = sharedPreferences.getString(TEXT, "");
        if (value.equals("")) {
            btnSave.setText(R.string.submit);
        } else {
            btnSave.setText(R.string.chnage_email);
        }
        btnSave.setOnClickListener(view -> {
            String ename = et_email.getText().toString();
            if (!ename.matches(emailPattern)) {
                et_email.setError("Enter valid email");
                et_email.requestFocus();
            } else {
                showtxt.setText(et_email.getText().toString());
                saveData();
            }
        });
        loadData();
        updateViews();
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEXT, showtxt.getText().toString());
        editor.apply();
        Toast.makeText(getApplicationContext(), "Saved!!", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");
        Log.e(TAG, text);
    }

    public void updateViews() {
        showtxt.setText(text);
    }

}