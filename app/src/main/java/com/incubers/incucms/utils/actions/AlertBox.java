package com.incubers.incucms.utils.actions;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import static com.incubers.incucms.utils.actions.VarHelper.MESSAGE_ID;
public class AlertBox extends AlertDialog {
    public AlertBox(Context context) {
        super(context);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(MESSAGE_ID);
        builder1.setPositiveButton(
                "Ok",
                (dialogInterface, i) -> dialogInterface.cancel());
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

