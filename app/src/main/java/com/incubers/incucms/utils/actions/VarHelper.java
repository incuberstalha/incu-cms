package com.incubers.incucms.utils.actions;

public class VarHelper {
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";
    public static final String TAG = "NOTE";
    public static String value;
    public static final String NO_INTERNET = "This app won't work if there is no internet";
    public static final String MESSAGE_ID = "We will notify you when mail send to your mail address";

}
