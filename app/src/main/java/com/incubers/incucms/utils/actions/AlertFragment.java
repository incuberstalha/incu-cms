package com.incubers.incucms.utils.actions;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.incubers.incucms.R;

public class AlertFragment extends DialogFragment {
    public static final String TITLE_ID = "title";
    public static final String MESSAGE_ID = "message";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Get supplied title and message body.
        Bundle messages = getArguments();
        Context context = getActivity();
        assert context != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(context.getString(R.string.error_ok_button_text), null);
        if (messages != null) {
            //Add the arguments. Supply a default in case the wrong key was used, or only one was set.
            builder.setTitle(messages.getString(TITLE_ID, "Sorry"));
            builder.setMessage(messages.getString(MESSAGE_ID, "There was an error."));
        } else {
            //Supply default text if no arguments were set.
            builder.setTitle("Sorry");
            builder.setMessage("There was an error.");
        }
        AlertDialog dialog = builder.create();
        return dialog;
    }
}

